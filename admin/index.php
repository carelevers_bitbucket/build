<?php 
session_start();
include '../config.php';


//load views
if(isset($_SESSION['logged_in'])){
	include 'php/init.php';
	include 'php/page.functions.php';
	include 'views/index.php';
}else{
	include 'views/login.php';
}

