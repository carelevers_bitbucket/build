<link href="views/css/admin_style.css" rel="stylesheet" type="text/css">
<link href="views/css/bootstrap.css" rel="stylesheet" type="text/css">
<link href="views/css/bootstrap-theme.css" rel="stylesheet" type="text/css">
<link href="views/css/simple-sidebar.css" rel="stylesheet" type="text/css">
<link href="views/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/angular.min.js"></script>
<script type="text/javascript" src="js/angular-animate.min.js"></script>
<script type="text/javascript" src="js/angular-route.min.js"></script> 
<script type="text/javascript" src="views/js/jquery-1.11.2.min.js"></script> 
<script type="text/javascript" src="views/js/bootstrap.js"></script> 
<script type="text/javascript" src="js/ajax_listener.js"></script>
<script type="text/javascript" src="views/js/app.js"></script>  
