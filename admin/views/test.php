<!DOCTYPE html>
<html ng-app="store">
<head>
<meta charset="UTF-8">
<title>Login BOBBUILD</title>
<?php include 'head.php'; ?>
<script type="text/javascript" src="js/app.js"></script> 
</head>
<body>
	<div class="list-group" ng-controller="StoreController as store">
		<div ng-repeat="product in store.products | orderBy:'-price'">
			<div ng-hide="product.soldOut">
				<h1>{{product.name | uppercase}}</h1>
				<h2>{{product.price | currency}}</h2>
				<p>{{product.description | limitTo:5}}</p>
				<button ng-show="product.canPurchase"> ADD to Cart </button>
			</div>
		</div>
	</div>
</body>
</html>